// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "RSPackage",
    platforms: [
        .iOS(.v11)
    ],
    products: [
        .library(name: "RSPackage", targets: ["RSPackage"]),
    ],
    dependencies: [
        .package(url: "https://github.com/Alamofire/Alamofire.git", .upToNextMajor(from: "5.2.0")),
        .package(url: "https://github.com/onevcat/Kingfisher.git", .upToNextMajor(from: "5.8.0"))
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(name: "RSPackage", dependencies: ["Alamofire", "Kingfisher"]),
        .testTarget(name: "RSPackageTests", dependencies: ["RSPackage"]),
    ]
)
