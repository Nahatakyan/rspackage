# Localization

**Usage**

Set your translation file name to 'Localizable.strings', than use '.localized' extension. Example: `"change_language".localized`

The language of the application can be changed from the application settings (starting with iOS 13). Use this to always check for selected language. 
```
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        // Check if language changed from settings
        Localize.checkForLanguageChange()

        return true
    }
```

Use embed views for realtime language changing. In package imlpemented:
> ABMButton, LocalizableButton, ABMLabel, LocalizableLabel, ABMTextField, LocalizableTextField

For font changing in all implemented views change `fontType` instead of `font`. You can use custom font for each language. You need to set it in `AppDelegate`
```
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        // Check if language changed from settings
        Localize.checkForLanguageChange()

        Localize.fontTypes = [
            FontType(code: "en", type: .regular, font: UIFont(name: "en_regular_font", size: 16)),
            FontType(code: "en", type: .semibold, font: UIFont(name: "en_semibold_font", size: 16)),
            FontType(code: "en", type: .bold, font: UIFont(name: "en_bold_font", size: 16)),
            FontType(code: "ru", type: .regular, font: UIFont(name: "ru_regular_font", size: 16)),
            FontType(code: "ru", type: .semibold, font: UIFont(name: "ru_semibold_font", size: 16)),
            FontType(code: "ru", type: .bold, font: UIFont(name: "ru_bold_font", size: 16)),
        ]

        return true
    }
```
To get available languages use

>Localize.availableLanguages


To get current language use

>Localize.currentLanguage


To change language use

>Localize.changeLanguage(languageCode: String)

or

>Localize.changeLanguage(language: Language)


**Example**

```
import UIKit
import Localize

class ViewController: UIViewController {

    @IBOutlet private weak var label: LocalizableLabel!
    @IBOutlet private weak var button: LocalizableButton!

    @IBAction private func changeLanguageButtonAction(_ sender: Any) {
        let alertController = UIAlertController(title: "change_language".localized, message: nil, preferredStyle: .actionSheet)

        Localize.availableLanguages.forEach { language in
            alertController.addAction(UIAlertAction(title: language.name, style: .default, handler: { (_) in
                Localize.changeLanguage(language: language)
            }))
        }

        present(alertController, animated: true, completion: nil)
    }
}
```

# Network requests

**Usage**

Package uses `Alamofire` for network requests. 
You need to create enum:
```
enum UserRequestService {
    case login
    case logout
}
```

If you need to send parameters with query or json input that parameters in this enum:
```
enum UserRequestService {
    case login(queryModel: Codable)
    case logout(jsonModel: Codable)
}
```
Then implement created enum to `RequestService` protocol and return all parameters for each request:
```
extension UserRequestService: RequestService {
    var path: String {
        switch self {
        case .login:
            return "api/user/login"
        case .logout:
            return "api/user/logout"
        }
    }

    var method: RequestMethod {
        switch self {
        case .login:
            return .post
        case .logout:
            return .post
        }
    }

    var params: NSDictionary? {
        switch self {
        case .login(let model):
            return model.dictionary
        case .logout(let model):
            return model.dictionary
        }
    }

    var encoding: RequestEncodingTypeEnum {
        switch self {
        case .login:
            return .query
        case .logout:
            return .applicationJson
        }
    }

    var headers: [String: String] {
        switch self {
        case .login:
            return ["device": "ios"]
        case .logout:
            return ["authkey": "key"]
        }
    }

    func errorResponse(_ error: NetworkManagerError) {
        switch error {
        case .authorizationError:
            // Log out user
            fatalError()
        default:
            break
        }
    }
}
```
If request uses parameters in query and in body(json), you can implement it like this:
```
enum UserRequestService {
    case login(queryModel: Codable, jsonModel: Codable)
}

extension UserRequestService: RequestService {
    var path: String {
        switch self {
        case .login(let queryModel, _):
            return "api/user/login\(queryModel.queryString)"
        }
    }

    var params: NSDictionary? {
        switch self {
        case .login(_, let jsonModel):
            return jsonModel.dictionary
        }
    }

    var encoding: RequestEncodingTypeEnum {
        switch self {
        case .login:
            return .applicationJson
        }
    }
}
```
Then from your ViewController or ViewModel you can call like this:
```
class ViewModel {
    func loginUser() {
        let model = UserLoginModel(login: "login", password: "password")
        UserRequestService.login(model: model).request { (response: NetworkManagerResult<Bool>) in
            switch response {
            case .success(let response):
                debugPrint("User loged in")
            case .failure(let error):
                debugPrint(error.message)
            }
        }
    }
}
```
In this case `error` type is enum and you can handle error how you want:
```
switch error {
case .authorizationError:
    debugPrint("Authorization error")
case .cancelled:
    debugPrint("Request cancelled")
case .noConnection:
    debugPrint("No internet connection")
case .serverError:
    debugPrint("Internal server error")
case .wrongResponseModel(let error, let response):
    debugPrint("Can't parse json to model:\n\(error)\nReseived json:\n\(response)")
case .unknownError(let code):
    debugPrint("Unknown error. Error code: \(code != nil ? "\(code!)" : "nil")")
case .custom(let message):
    debugPrint("Custom error: \(message)")
}
```

# Extensions

**Bundle**
- `icon` - Get application icon
- `releaseVersionNumber` - Release version
- `buildVersionNumber` - Build version

**CACornerMask**
- `topLeft` - Minimum X, minimum Y corner
- `topRight` - Maximum X, minimum Y corner
- `bottomLeft` - Minimum X, maximum Y corner
- `bottomRight` - Maximum X, maximum Y corner

**Data**
- `text` - Print data in JSON format

**Encodable**
- `dictionary` - Convert Codable model to NSDictionary
- `text` - Print Codable model in JSON format
- `queryString` - Convert Codable model properties to query string

**NSDictionary**
- `text` - Print dictionary in JSON format

**UIApplication**
- `topViewController` - Returns current presented view controller
- `isKeyboardPresented` - Boolean value that says keyboard shown or not

**UIColor**
- `init(hex: String)` - Get color from HEX value
- `init(darkMode: UIColor, lightMode: UIColor)` - Get different colors for different user interface style

**UIDevice**
- `modelName` - Get device model name

**UIImageView**
- `setImage` - Set image from web url (Kingfisher)
- `changeImage` - Change image with cross disolve animation. Supported content modes` scaleAspectFit, scaleAspectFill, scaleToFill

**UIResponder**
- `name` - Returns Class name in String

**UIView**
- `roundCorners` - Corner radius for custom corners
- `applyShadow` - Set shadow to view layer
- `removeShadow` - Remove shadow from view layer
- `addRatioConstraint` - Add ratio constraint to view
- `pinEdgesToSuperView` - Add top, left, right, bottom constraints to superview
- `pinCenterToSuperView` - Add center x, y constraints to superview
- `pinEdgesToSuperViewSafeXMargin` - Add top, left, right, bottom constraints to superview. Left and right anchors are connected to superview safearea anchors.
- `asImage` - Return view as image
