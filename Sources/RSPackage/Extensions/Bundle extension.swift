//
//  Bundle extension.swift
//  Tapon.am
//
//  Created by Ruben Nahatakyan on 1/10/20.
//  Copyright © 2020 Ruben Nahatakyan. All rights reserved.
//

import UIKit

public extension Bundle {

    // MARK: - Icon
    var icon: UIImage? {
        guard
            let dictionary = infoDictionary as NSDictionary?,
            let icons = dictionary.value(forKey: "CFBundleIcons") as? NSDictionary,
            let primaryIcon = icons.value(forKey: "CFBundlePrimaryIcon") as? NSDictionary,
            let iconFiles = primaryIcon.value(forKey: "CFBundleIconFiles") as? [String],
            let lastIcon = iconFiles.last
            else { return nil }
        return UIImage(named: lastIcon)
    }

    // MARK: - Release version
    var releaseVersionNumber: String {
        return infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
    }

    // MARK: - Build version
    var buildVersionNumber: String {
        return infoDictionary?["CFBundleVersion"] as? String ?? ""
    }
}
