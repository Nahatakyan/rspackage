//
//  CACornerMas.swift
//  Grand Candy
//
//  Created by MacBook Pro on 4/7/20.
//  Copyright © 2020 Ruben Nahatakyan. All rights reserved.
//

import UIKit

public extension CACornerMask {
    static var topLeft: CACornerMask {
        return .layerMinXMinYCorner
    }

    static var topRight: CACornerMask {
        return .layerMaxXMinYCorner
    }

    static var bottomLeft: CACornerMask {
        return .layerMinXMaxYCorner
    }

    static var bottomRight: CACornerMask {
        return .layerMaxXMaxYCorner
    }
}
