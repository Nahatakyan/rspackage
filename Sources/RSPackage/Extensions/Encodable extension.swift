//
//  Encodable extension.swift
//  Flowerry
//
//  Created by Ruben Nahatakyan on 9/14/19.
//  Copyright © 2019 Ruben Nahatakyan. All rights reserved.
//

import Foundation

public extension Encodable {
    var dictionary: NSDictionary? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        return (try? JSONSerialization.jsonObject(with: data, options: [.allowFragments, .mutableLeaves])).flatMap { $0 as? NSDictionary }
    }

    var text: NSString {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        return (try? encoder.encode(self).text) ?? ""
    }

    var queryString: String {
        guard let dictionary = self.dictionary else { return "" }
        return "?" + dictionary.map { "\($0.key)=\($0.value)" }.joined(separator: "&")
    }
}
