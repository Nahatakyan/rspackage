//
//  NSDictionary extension.swift
//  Grand Candy
//
//  Created by Ruben Nahatakyan on 7/1/20.
//  Copyright © 2020 Ruben Nahatakyan. All rights reserved.
//

import Foundation

public extension NSDictionary {

    var text: NSString {
        return (try? JSONSerialization.data(withJSONObject: self, options: .prettyPrinted))?.text ?? ""
    }
}
