//
//  UIImageView extension.swift
//
//  Created by Ruben Nahatakyan on 7/28/20.
//

import UIKit
import Kingfisher

public extension UIImageView {

    // MARK: - Image from web
    func setImage(urlString: String?, placeholder: UIImage? = nil, completed: (() -> Void)? = nil) {
        image = placeholder
        guard let string = urlString else { return }
        kf.indicatorType = .activity
        kf.setImage(with: URL(string: string), placeholder: placeholder, completionHandler:  { (_) in
            completed?()
        })
    }

    // MARK: - Change image with animation
    func changeImage(image: UIImage?, duration: Double = 1.0) {
        let oldLayer = getLayerForImage(self.image)
        oldLayer?.opacity = 0

        let newLayer = getLayerForImage(image)
        newLayer?.opacity = 0

        // Create layers from images
        CATransaction.begin()
        CATransaction.setDisableActions(true)

        if let layer = oldLayer {
            self.layer.insertSublayer(layer, at: 0)
        }

        if let layer = newLayer {
            self.layer.insertSublayer(layer, at: 0)
        }

        CATransaction.commit()
        self.image = nil

        // Changes layers alphas with animation
        let hideLayerAnimation = CABasicAnimation(keyPath: "opacity")
        hideLayerAnimation.toValue = 0
        hideLayerAnimation.fromValue = 1
        hideLayerAnimation.duration = duration
        oldLayer?.add(hideLayerAnimation, forKey: "hide_layer")
        oldLayer?.opacity = 0

        let showLayerAnimation = CABasicAnimation(keyPath: "opacity")
        showLayerAnimation.toValue = 1
        showLayerAnimation.fromValue = 0
        showLayerAnimation.duration = duration
        newLayer?.add(showLayerAnimation, forKey: "hide_layer")
        newLayer?.opacity = 1

        DispatchQueue.main.asyncAfter(deadline: .now() + duration) {
            self.image = image
            oldLayer?.removeFromSuperlayer()
            newLayer?.removeFromSuperlayer()
        }
    }

    // MARK: Get layer for image
    private func getLayerForImage(_ newImage: UIImage?) -> CALayer? {
        guard let image = newImage else { return nil }
        let rect: CGRect

        switch contentMode {
        case .scaleAspectFit:
            let multiplayer = max(image.size.width / frame.width, image.size.height / frame.height)
            let size = CGSize(width: image.size.width / multiplayer, height: image.size.height / multiplayer)
            let origin = CGPoint(x: (frame.width - size.width) / 2, y: (frame.height - size.height) / 2)
            rect = CGRect(origin: origin, size: size)
        case .scaleAspectFill:
            let multiplayer = min(image.size.width / frame.width, image.size.height / frame.height)
            let size = CGSize(width: image.size.width / multiplayer, height: image.size.height / multiplayer)
            let origin = CGPoint(x: (frame.width - size.width) / 2, y: (frame.height - size.height) / 2)
            rect = CGRect(origin: origin, size: size)
        default:
            rect = self.bounds
        }

        let layer = CALayer()
        layer.frame = rect
        layer.contents = image.cgImage
        return layer
    }
}
