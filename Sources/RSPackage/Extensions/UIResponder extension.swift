//
//  UIResponder extension.swift
//  Grand Candy
//
//  Created by MacBook Pro on 4/7/20.
//  Copyright © 2020 Ruben Nahatakyan. All rights reserved.
//

import UIKit

public extension UIResponder {
    class var name: String {
        return String(describing: self)
    }
}
