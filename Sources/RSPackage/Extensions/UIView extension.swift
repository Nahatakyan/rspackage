//
//  UIView extensions.swift
//  Grand Candy
//
//  Created by MacBook Pro on 4/7/20.
//  Copyright © 2020 Ruben Nahatakyan. All rights reserved.
//

import UIKit

public extension UIView {

    // MARK: - Round corners
    func roundCorners(_ corners: CACornerMask, radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.maskedCorners = corners
    }

    // MARK: - Shadow
    func applyShadow(animated: Bool = false, color: UIColor = UIColor(white: 0, alpha: 0.2), x: CGFloat = 0, y: CGFloat = 3, blur: CGFloat = 6) {
        removeShadow(animated: false)
        if animated {
            let animation = CABasicAnimation(keyPath: #keyPath(CALayer.shadowOpacity))
            animation.fromValue = 0
            animation.toValue = 1
            animation.duration = 0.2
            layer.add(animation, forKey: animation.keyPath)
        }
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = 1
        layer.shadowOffset = CGSize(width: x, height: y)
        layer.shadowRadius = blur
    }

    func removeShadow(animated: Bool = false) {
        guard layer.shadowOpacity != 0 else { return }
        if animated {
            let animation = CABasicAnimation(keyPath: #keyPath(CALayer.shadowOpacity))
            animation.fromValue = layer.shadowOpacity
            animation.toValue = 0.0
            animation.duration = 0.2
            layer.add(animation, forKey: animation.keyPath)
        }

        layer.shadowOpacity = 0
    }

    // MARK: - Constraint
    @discardableResult
    func addRatioConstraint(multiplayer: CGFloat = 1, priority: UILayoutPriority = .required) -> NSLayoutConstraint {
        let constraint = NSLayoutConstraint(item: self, attribute: .width, relatedBy: .equal, toItem: self, attribute: .height, multiplier: multiplayer, constant: 0)
        constraint.priority = priority
        constraint.isActive = true
        return constraint
    }

    func pinEdgesToSuperView(xMargin: CGFloat = 0, yMargin: CGFloat = 0) {
        guard let superView = superview else { return }
        translatesAutoresizingMaskIntoConstraints = false
        topAnchor.constraint(equalTo: superView.topAnchor, constant: yMargin).isActive = true
        leftAnchor.constraint(equalTo: superView.leftAnchor, constant: xMargin).isActive = true
        rightAnchor.constraint(equalTo: superView.rightAnchor, constant: -xMargin).isActive = true
        bottomAnchor.constraint(equalTo: superView.bottomAnchor, constant: -yMargin).isActive = true
    }

    func pinCenterToSuperView() {
        guard let superView = superview else { return }
        translatesAutoresizingMaskIntoConstraints = false
        centerXAnchor.constraint(equalTo: superView.centerXAnchor).isActive = true
        centerYAnchor.constraint(equalTo: superView.centerYAnchor).isActive = true
    }

    func pinEdgesToSuperViewSafeXMargin(xMargin: CGFloat = 0, yMargin: CGFloat = 0) {
        guard let superView = superview else { return }
        translatesAutoresizingMaskIntoConstraints = false
        topAnchor.constraint(equalTo: superView.topAnchor, constant: yMargin).isActive = true
        bottomAnchor.constraint(equalTo: superView.bottomAnchor, constant: -yMargin).isActive = true
        leftAnchor.constraint(equalTo: superView.safeAreaLayoutGuide.leftAnchor, constant: xMargin).isActive = true
        rightAnchor.constraint(equalTo: superView.safeAreaLayoutGuide.rightAnchor, constant: -xMargin).isActive = true
    }

    // MARK: - Image
    var asImage: UIImage? {
        UIGraphicsBeginImageContextWithOptions(self.frame.size, false, 0)
        guard let context = UIGraphicsGetCurrentContext() else {
            UIGraphicsEndImageContext()
            return nil
        }
        self.layer.render(in: context)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        guard let cgImage = image?.cgImage else { return nil }
        return UIImage(cgImage: cgImage)
    }
}
