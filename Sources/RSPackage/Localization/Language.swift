//
//  Language.swift
//
//  Created by Ruben Nahatakyan on 7/30/20.
//

import Foundation

public struct Language: Equatable {
    public let code: String
    public let engName: String?
    public let nativeName: String?

    public init(code: String) {
        self.code = code
        self.nativeName = Locale(identifier: code).localizedString(forLanguageCode: code)?.firstUppercased

        let languageCode = Locale.components(fromIdentifier: code)[NSLocale.Key.languageCode.rawValue] ?? ""
        self.engName = Locale(identifier: "en").localizedString(forLanguageCode: languageCode)?.firstUppercased
    }

    public static func == (lhs: Language, rhs: Language) -> Bool {
        return lhs.code == rhs.code
    }
}
