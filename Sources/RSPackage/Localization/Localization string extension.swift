//
//  Localization string extension.swift
//
//  Created by Ruben Nahatakyan on 7/30/20.
//

import Foundation

public extension String {
    var localized: String {
        guard let path = Bundle.main.path(forResource: Localize.currentLanguage?.code, ofType: "lproj"), let bundle = Bundle(path: path) else { return self }
        return NSLocalizedString(self, tableName: nil, bundle: bundle, comment: "")
    }

    func localize(to languageCode: String) -> String {
        guard let path = Bundle.main.path(forResource: languageCode, ofType: "lproj"), let bundle = Bundle(path: path) else { return self }
        return NSLocalizedString(self, tableName: nil, bundle: bundle, comment: "")
    }
}
