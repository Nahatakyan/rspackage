//
//  Localize.swift
//
//  Created by Ruben Nahatakyan on 7/30/20.
//

import UIKit

open class Localize: NSObject {

    // MARK: Default language
    public static var defaultLanguageCode: String? = Locale.current.languageCode

    // MARK: - Available languages
    open class var availableLanguages: [Language] {
        var availableLanguages = Bundle.main.localizations
        if let index = availableLanguages.firstIndex(of: "Base") {
            availableLanguages.remove(at: index)
        }
        return availableLanguages.map { Language(code: $0) }
    }

    // MARK: - Current language code
    open class var currentLanguage: Language? {
        if let currentLanguage = UserDefaults.standard.string(forKey: LocalizeUserDefaultKey) {
            return Language(code: currentLanguage)
        }
        guard let currentLanguageCode = defaultLanguageCode else { return nil }
        UserDefaults.standard.set(currentLanguageCode, forKey: LocalizeUserDefaultKey)
        return Language(code: currentLanguageCode)
    }

    // MARK: - Change language
    open class func changeLanguage(language: Language) {
        guard availableLanguages.contains(language), currentLanguage != language else { return }

        var appleLanguageIdentifiers = Locale.preferredLanguages
        let languageCodes = appleLanguageIdentifiers.map { identifier -> String in
            let components = Locale.components(fromIdentifier: identifier)
            return components[NSLocale.Key.languageCode.rawValue] ?? ""
        }

        if let selectedIndex = languageCodes.firstIndex(of: language.code) {
            let languageIdentifier = appleLanguageIdentifiers[selectedIndex]
            appleLanguageIdentifiers.remove(at: selectedIndex)
            appleLanguageIdentifiers.insert(languageIdentifier, at: 0)
        } else {
            appleLanguageIdentifiers.insert(language.code, at: 0)
        }

        UserDefaults.standard.set(language.code, forKey: LocalizeUserDefaultKey)
        UserDefaults.standard.set(appleLanguageIdentifiers, forKey: "AppleLanguages")
        NotificationCenter.default.post(name: .ApplicationLanguageChanged, object: nil)
    }

    // MARK: - Change language with code
    open class func changeLanguage(languageCode: String) {
        let language = Language(code: languageCode)
        changeLanguage(language: language)
    }

    // MARK: - Check for language change
    open class func checkForLanguageChange() {
        guard let changedLanguage = Locale.preferredLanguages.first, let currentLanguage = UserDefaults.standard.string(forKey: LocalizeUserDefaultKey) else { return }
        let components = Locale.components(fromIdentifier: changedLanguage)
        let languageCode = components[NSLocale.Key.languageCode.rawValue] ?? ""
        guard languageCode != currentLanguage else { return }
        changeLanguage(languageCode: languageCode)
    }

    // MARK: - Font types
    public static var fontTypes: [FontType] = []
}

