//
//  Public properties.swift
//
//  Created by Ruben Nahatakyan on 7/30/20.
//

import Foundation

public let LocalizeUserDefaultKey = "ra_language_code"

public extension Notification.Name {
    static let ApplicationLanguageChanged = Notification.Name("application_language_changed")
}

public extension String {
    var firstUppercased: String {
        return prefix(1).uppercased() + self.lowercased().dropFirst()
    }
}
