//
//  Font type enum.swift
//
//  Created by Ruben Nahatakyan on 7/30/20.
//

import UIKit

public enum FontTypeEnum {
    case regular
    case semibold
    case bold

    var identifier: String {
        switch self {
        case .regular: return "font_type_regular"
        case .semibold: return "font_type_semibold"
        case .bold: return "font_type_bold"
        }
    }
}

public enum FontTypeSizedEnum {
    case regular(fontSize: CGFloat = 16)
    case semibold(fontSize: CGFloat = 16)
    case bold(fontSize: CGFloat = 16)

    public var font: UIFont {
        let language = Localize.currentLanguage?.code ?? "en"
        let type = (Localize.fontTypes.first { $0.code == language && $0.type.identifier == self.identifier }) ?? FontType()

        switch self {
        case .regular(let size), .semibold(let size), .bold(let size):
            return type.font.withSize(size)
        }
    }

    var identifier: String {
        switch self {
        case .regular: return "font_type_regular"
        case .semibold: return "font_type_semibold"
        case .bold: return "font_type_bold"
        }
    }
}
