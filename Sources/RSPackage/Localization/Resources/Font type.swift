//
//  Font type.swift
//
//  Created by Ruben Nahatakyan on 7/30/20.
//

import UIKit

public struct FontType {
    public let code: String
    public let type: FontTypeEnum
    public let font: UIFont

    init() {
        self.code = "en"
        self.type = .regular
        self.font = UIFont.systemFont(ofSize: UIFont.systemFontSize)
    }

    public init(code: String, type: FontTypeEnum, font: UIFont) {
        self.code = code
        self.type = type
        self.font = font
    }
}
