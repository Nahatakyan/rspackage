//
//  Localizable button.swift
//
//  Created by Ruben Nahatakyan on 7/30/20.
//

import UIKit

open class LocalizableButton: RSButton {

    // MARK: - Properties
    private var localizableTitles: [(state: UIControl.State, title: String?)] = []

    open override var isEnabled: Bool {
        willSet {
            alpha = newValue ? 1 : 0.5
        }
    }

    // MARK: - Init
    override public init(frame: CGRect) {
        super.init(frame: frame)
        startupSetup()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        startupSetup()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

// MARK: - Startup default setup
private extension LocalizableButton {
    @objc func startupSetup() {
        setTitle(title(for: .normal), for: .normal)
        setTitle(title(for: .highlighted), for: .highlighted)
        setTitle(title(for: .selected), for: .selected)
        setTitle(title(for: .disabled), for: .disabled)
    }
}

// MARK: - Public methods
extension LocalizableButton {
    override open func setTitle(_ title: String?, for state: UIControl.State) {
        localizableTitles.append((state: state, title: title))
        super.setTitle(title?.localized, for: state)
    }
}

// MARK: - Actions
extension LocalizableButton {


    // MARK: Language changed
    open override func languageChanged() {
        localizableTitles.forEach {
            super.setTitle($0.title?.localized, for: $0.state)
        }
    }
}
