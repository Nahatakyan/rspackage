//
//  Localizable label.swift
//
//  Created by Ruben Nahatakyan on 7/30/20.
//

import UIKit

open class LocalizableLabel: RSLabel {

    // MARK: - Properties
    private var localizableString: String?

    override open var text: String? {
        set {
            localizableString = newValue
            super.text = localizableString?.localized
        }
        get {
            return super.text
        }
    }

    // MARK: - Init
    override public init(frame: CGRect) {
        super.init(frame: frame)
        startup()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        localizableString = text
        startup()
    }
}

// MARK: - Startup
private extension LocalizableLabel {
    func startup() {
        text = localizableString
    }
}

// MARK: - Actions
extension LocalizableLabel {


    // MARK: Language changed
    open override func languageChanged() {
        text = localizableString
    }
}
