//
//  Localizable text field.swift
//
//  Created by Ruben Nahatakyan on 7/30/20.
//

import UIKit

open class LocalizableTextField: RSTextField {

    // MARK: - Properties
    private(set) var localizedPlaceholder: String?

    override open var placeholder: String? {
        set {
            localizedPlaceholder = newValue
            let attributes: [NSAttributedString.Key: UIColor] = [.foregroundColor: placeholderColor]
            self.attributedPlaceholder = NSAttributedString(string: newValue?.localized ?? "", attributes: attributes)
        }
        get {
            return self.attributedPlaceholder?.string
        }
    }

    open override var isEnabled: Bool {
        willSet {
            alpha = newValue ? 1 : 0.5
        }
    }

    open var placeholderColor: UIColor = UIColor(hex: "#7D7D7D").withAlphaComponent(0.5) {
        didSet {
            placeholder = localizedPlaceholder
        }
    }

    // MARK: - Init
    override public init(frame: CGRect) {
        super.init(frame: frame)
        startup()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        localizedPlaceholder = placeholder
        startup()
    }
}

// MARK: - Startup
private extension LocalizableTextField {
    func startup() {
        languageChanged()
    }
}

// MARK: - Actions
extension LocalizableTextField {
    open override func languageChanged() {
        placeholder = localizedPlaceholder
    }
}
