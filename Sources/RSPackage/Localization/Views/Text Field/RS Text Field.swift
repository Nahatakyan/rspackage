//
//  RS Text Field.swift
//
//  Created by Ruben Nahatakyan on 7/30/20.
//

import UIKit

open class RSTextField: UITextField {

    // MARK: - Properties
    public var fontType: FontTypeSizedEnum = .regular(fontSize: 16) {
        didSet {
            updateFont()
        }
    }

    // MARK: - Init
    public override init(frame: CGRect) {
        super.init(frame: .zero)
        startup()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        startup()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

// MARK: - Startup
private extension RSTextField {
    func startup() {
        addNotificationListeners()

        fontType = .regular(fontSize: 16)
    }

    // MARK: Notification listeners
    func addNotificationListeners() {
        NotificationCenter.default.addObserver(self, selector: #selector(applicationLanguageChanged), name: .ApplicationLanguageChanged, object: nil)
    }
}

// MARK: - Actions
private extension RSTextField {


    // MARK: Language changed
    @objc func applicationLanguageChanged() {
        updateFont()
        languageChanged()
    }

    // MARK: Update font
    func updateFont() {
        self.font = fontType.font
    }
}


// MARK: - Public methods
extension RSTextField {
    @objc open func languageChanged() { }
}
