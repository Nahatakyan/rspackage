//
//  User login model.swift
//
//  Created by Ruben Nahatakyan on 7/28/20.
//

import Foundation

class UserLoginModel: Codable {
    let login: String
    let password: String

    init(login: String, password: String) {
        self.login = login
        self.password = password
    }
}
