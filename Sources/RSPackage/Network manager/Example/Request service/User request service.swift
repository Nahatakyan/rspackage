//
//  User request service.swift
//
//  Created by Ruben Nahatakyan on 7/28/20.
//

import Foundation

enum UserRequestService {
    case login(model: Codable)
    case logout
}

extension UserRequestService: RequestService {
    var path: String {
        switch self {
        case .login:
            return "api/user/login"
        case .logout:
            return "api/user/logout"
        }
    }

    var method: RequestMethod {
        switch self {
        case .login:
            return .post
        case .logout:
            return .get
        }
    }

    var params: NSDictionary? {
        switch self {
        case .login(let model):
            return model.dictionary
        case .logout:
            return nil
        }
    }

    var encoding: RequestEncodingTypeEnum {
        switch self {
        case .login:
            return .query
        case .logout:
            return .applicationJson
        }
    }

    var headers: [String: String] {
        switch self {
        case .login:
            return ["device": "ios"]
        case .logout:
            return ["authkey": "key"]
        }
    }

    func errorResponse(_ error: NetworkManagerError) {
        switch error {
        case .authorizationError:
            // Log out user
            fatalError()
        default:
            break
        }
    }
}
