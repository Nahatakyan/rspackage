//
//  Network manager example view.swift
//
//  Created by Ruben Nahatakyan on 7/28/20.
//

import UIKit

class NetworkManagerExampleView: UIView {


    func loginUser() {
        let model = UserLoginModel(login: "login", password: "password")
        UserRequestService.login(model: model).request { (response: NetworkManagerResult<Bool>) in
            switch response {
            case .success(_):
                debugPrint("User loged in")
            case .failure(let error):
                debugPrint(error.message)
            }
        }
    }

    func upload() {
        UserRequestService.logout.upload(files: (name: "File", data: [UIImage().jpegData(compressionQuality: 0.5)!]), fileType: .photo) { (response: NetworkManagerResult<String>) in
            switch response {
            case .success(let responseText):
                debugPrint(responseText)
            case .failure(let error):
                debugPrint(error.message)
            }
        }

        Localize.fontTypes = [
            FontType(code: "hy", type: .regular, font: UIFont.systemFont(ofSize: 12))
        ]
    }
}
