//
//  Request service.swift
//  Grand Candy
//
//  Created by Ruben Nahatakyan on 6/1/20.
//  Copyright © 2020 Ruben Nahatakyan. All rights reserved.
//

import Foundation
import Alamofire

public protocol RequestService {
    var path: String { get }
    var method: RequestMethod { get }
    var params: NSDictionary? { get }
    var encoding: RequestEncodingTypeEnum { get }
    var headers: [String: String] { get }
    func errorResponse(_ error: NetworkManagerError)
}

extension RequestService {

    @discardableResult
    public func request<T: Codable>(result: @escaping(NetworkManagerResult<T>) -> Void) -> DataRequest? {
        let request = NetworkManager.shared.request(method: method.AFMethod, endpoint: path, params: params, encoding: encoding, headers: HTTPHeaders(headers)) { (response: NetworkManagerResult<T>) in
            switch response {
            case .success(_): break
            case .failure(let error):
                self.errorResponse(error)
            }
            result(response)
        }
        return request
    }

    @discardableResult
    public func upload<T: Codable>(files: (name: String, data: [Data]), fileType: UploadFileTypeEnum, result: @escaping(NetworkManagerResult<T>) -> Void) -> DataRequest? {
        let request = NetworkManager.shared.upload(method: method.AFMethod, endpoint: path, params: params, files: files, fileType: fileType, headers: HTTPHeaders(headers)) { (response: NetworkManagerResult<T>) in
            switch response {
            case .success(_): break
            case .failure(let error):
                self.errorResponse(error)
            }
            result(response)
        }
        return request
    }
}
