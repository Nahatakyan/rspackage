//
//  ResponseModel.swift
//  Grand Candy
//
//  Created by Ruben Nahatakyan on 6/1/20.
//  Copyright © 2020 Ruben Nahatakyan. All rights reserved.
//

import Foundation

class ResponseModel<T: Codable>: Codable {
    let success: Bool
    let data: T
    let message: String?
    let messages: [Messages]?
}

public class Messages: Codable {
    public var key: String
    public var message: String
}
