import XCTest

import RSPackageTests

var tests = [XCTestCaseEntry]()
tests += RSPackageTests.allTests()
XCTMain(tests)
